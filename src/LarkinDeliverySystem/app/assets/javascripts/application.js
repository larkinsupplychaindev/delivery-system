// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require js/plugin/pace/pace.min.js
//= require js/libs/jquery-2.1.1.min.js
// require jquery
// require jquery_ujs
// require turbolinks
// require_tree .
//= require js/libs/jquery-ui-1.10.3.min.js
//= require js/app.config.js
//= require js/plugin/jquery-touch/jquery.ui.touch-punch.min.js
//= require js/bootstrap/bootstrap.min.js
//= require js/notification/SmartNotification.min.js
//= require js/smartwidgets/jarvis.widget.min.js
// require js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js
//= require js/plugin/sparkline/jquery.sparkline.min.js
//= require js/plugin/jquery-validate/jquery.validate.min.js
//= require js/plugin/masked-input/jquery.maskedinput.min.js
//= require js/plugin/select2/select2.min.js
//= require js/plugin/bootstrap-slider/bootstrap-slider.min.js
//= require js/plugin/msie-fix/jquery.mb.browser.min.js
//= require js/plugin/fastclick/fastclick.min.js
// require js/demo.min.js
//= require js/app.min.js
//= require js/speech/voicecommand.min.js
//= require js/smart-chat-ui/smart.chat.ui.min.js
//= require js/smart-chat-ui/smart.chat.manager.min.js


