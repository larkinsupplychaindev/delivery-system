module TripsHelper
  def sortable(column, title = nil)
    sortable_column params, column, title, &@build_path
  end
end
