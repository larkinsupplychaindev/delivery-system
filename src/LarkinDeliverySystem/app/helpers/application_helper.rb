module ApplicationHelper
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def sortable_column(cur_params, column, title, &url)
    title ||= column.titleize
    original_sort_arr = cur_params[:s] || []

    index = original_sort_arr.index { |x| x[:f] == column }
    direction = 'a'
    if index && index >= 0
      s = original_sort_arr[index]
      direction = s[:d] == 'd' ? 'a' : 'd'
      sort_order = index + 1
      asc = s[:d] == 'a'
      desc = s[:d] == 'd'
    end

    sort_arr = []
    sort_arr << {:f => column, :d => direction}
    original_sort_arr.each { |s| sort_arr << {:f => s[:f], :d => s[:d]} if s[:f] != column }

    url = url.call(cur_params.merge(:s => sort_arr))

    render partial: 'sortable_column', locals: {title: title, url: url, sort_order: sort_order, asc: asc, desc: desc}
  end
end
