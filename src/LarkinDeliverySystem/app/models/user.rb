class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, #:registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :users_roles
  has_many :roles, :through => :users_roles

  def self.available_drivers_on_date_and_time_shift(date, time_shift, driver_id = nil)
    date ||= nil
    time_shift ||= nil

    return User.none unless date && time_shift && time_shift != ''

=begin
  | - означает включительно start - дата начала периода end - дата окончания периода (смены)
 start             end
  | 1 смена Driver1 |
                    | 2 смена |
              driver_start                                 driver_end
                    | отдых   | 3 смена Driver1 | отдых          |
                                                | ночь |
                                                       | 1 смена |
                                                                 | 2 смена Driver1 |
                                                                                   | 3 смена |

  все это не для себя, а для других разработчиков
  условие на естественном языке: если дата начала периода и дата окончания периода смены для перевозки X попадают
  в период планируемой перевозки с учетом того что водитель должен отдыхать (т.е. -1 одна смена от планируемой и +1 смена к планируемой)
  то такие водители не доступны для создания перевозки с указанной датой и временным сдвигом

  Если предположить, что грузовики обслуживаются и загружатся в течении той же смены, когда выполнят рейс, то truck_start и truck_end будут равны периоды смены
  тогда можно будет отправлять грузовики в рейс каждую смену при условии найма дополнительных водителей (так как водители все таки должны отдыхать)
=end

# TODO: запрос ниже не оптимален по определению, его лучше переделать на join между trips и delivery_shifts
# (на конкретный временной сдвиг указанный в параметре) и избавиться от использования функции get_timestamp_by_shift,
# а просто сразу прибавлять интервалы + можно избавиться от одного and если вместо проверки start end смены просто проверять середину периода смены.

    @drivers = User.joins(:roles).where(roles: {name: :Driver}).where("not exists(
select 1
from trips tr
where tr.driver_id = users.id
  and public.get_timestamp_by_shift(tr.date, tr.time_shift_code, 'start')
    between public.get_timestamp_by_shift(:date, :time_shift_code, 'truck_start')
        and public.get_timestamp_by_shift(:date, :time_shift_code, 'truck_end')
  and public.get_timestamp_by_shift(tr.date, tr.time_shift_code, 'end')
    between public.get_timestamp_by_shift(:date, :time_shift_code, 'truck_start')
        and public.get_timestamp_by_shift(:date, :time_shift_code, 'truck_end')
)", {date: date, time_shift_code: time_shift})

    @drivers = @drivers.where id: driver_id if driver_id

    @drivers
  end
end
