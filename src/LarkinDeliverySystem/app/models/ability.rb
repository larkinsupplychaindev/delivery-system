class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)
    if user.roles.find_by name: 'Admin'
      can :manage, [Truck, DeliveryShift, Role, User, UsersRole]
    elsif user.roles.find_by name: 'Dispatcher'
      can [:index, :show, :upload_view, :upload], [Order]
      can [:edit, :update, :split], [OrderDelivery]
      can [:index, :show, :new, :edit, :create, :update, :destroy], [Trip]
      can [:create, :update, :destroy], [TripCargo]
      can [:index, :show], [Truck]
    elsif user.roles.find_by name: 'Driver'
      can [:index_my, :show, :export_to_txt, :export_to_pdf], [Trip]
      can [:index, :show], [Truck]
    end

    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
