class Pagination
  def initialize(params, param_name, buttons_count, rows_count_on_page, &url)
    @params = params
    @cur_page_index = params[param_name].to_i if params[param_name]
    @cur_page_index = @cur_page_index || 1

    @buttons_count = buttons_count
    @buttons_count = @buttons_count || 10

    @rows_count_on_page = rows_count_on_page
    @rows_count_on_page = @rows_count_on_page || 5

    @total_rows_count = 0

    @buttons = []


    @param_name = param_name
    @url = url
  end

  def cur_page_index
    @cur_page_index
  end

  def count_on_page
    @rows_count_on_page
  end

  def limit
    @rows_count_on_page
  end

  def offset
    (@cur_page_index - 1) * limit
  end

  def total_rows_count=(total_rows_count)
    @total_rows_count = total_rows_count
    create_buttons
  end


  def buttons
    @buttons
  end

  def get_url(page_index)
    @url.call(@params.merge(@param_name => page_index))
  end

  private

  def create_buttons
    # 1 ... 11 12 13 14 15 16 17 18 19 20 ... 100

    @buttons = []
    all_page = @total_rows_count / @rows_count_on_page + (@total_rows_count % @rows_count_on_page > 0 ? 1 : 0)

    cur_buttons_group = (@cur_page_index - 1) / @buttons_count
    end_buttons_group = (all_page - 1) / @buttons_count

    if 0 < cur_buttons_group
      @buttons << PaginationButton.new(self, 1, "1")

      prev_group = cur_buttons_group * @buttons_count
      @buttons << PaginationButton.new(self, prev_group, "...") if prev_group > 1
    end

    start_page_index = (cur_buttons_group * @buttons_count) + 1
    end_page_index = start_page_index + @buttons_count - 1
    end_page_index = all_page if all_page < end_page_index

    (start_page_index..end_page_index).each do |index|
      @buttons << PaginationButton.new(self, index, "#{index}")
    end


    if cur_buttons_group < end_buttons_group
      next_group = (cur_buttons_group + 1) * @buttons_count + 1
      @buttons << PaginationButton.new(self, next_group, "...") if next_group < all_page

      @buttons << PaginationButton.new(self, all_page, "#{all_page}")
    end
  end
end