class Notice
  SUCCESS = 1
  INFO = 2
  WARNING = 3
  ERROR = 4

  def initialize(type, message)
    unless (1..4) === type
      raise "Not supported notice type: #{type}"
    end

    @type = type
    @message = message
  end

  def type
    @type
  end

  def message
    @message
  end
end