class PaginationButton

  def initialize(pagination, page_index, name)
    @pagination = pagination
    @page_index = page_index
    @name = name
  end

  def is_active
    @pagination.cur_page_index == @page_index
  end

  def url
    @pagination.get_url @page_index
  end

  def name
    @name
  end
end