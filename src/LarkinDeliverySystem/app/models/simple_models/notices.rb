class Notices

  def initialize
    @notices = []
  end

  def notices
    @notices
  end

  def success(msg)
    @notices << Notice.new(Notice::SUCCESS, msg)
  end

  def warning(msg)
    @notices << Notice.new(Notice::WARNING, msg)
  end

  def info(msg)
    @notices << Notice.new(Notice::INFO, msg)
  end

  def error(msg)
    @notices << Notice.new(Notice::ERROR, msg)
  end
end