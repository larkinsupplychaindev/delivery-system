class Order < ApplicationRecord
  belongs_to :origin_address, :class_name => 'Address', :foreign_key => 'origin_address_id', :validate => true
  belongs_to :destination_address, :class_name => 'Address', :foreign_key => 'destination_address_id', :validate => true

  has_many :deliveries, :class_name => 'OrderDelivery'

  def next_delivery_date
    self['next_delivery_date']
  end

  def last_delivered_date
    self['last_delivered_date']
  end

  def is_from_client_to_stock
    # признак возврата
    #is_from_stock_to_client = Address.address_stock_names.include?(origin_address.name) # доставка
    is_from_client_to_stock = Address.address_stock_names.include?(destination_address.name) # возврат
    is_from_client_to_stock
  end

  def is_from_stock_to_client
    # признак доставки
    !is_from_client_to_stock # если не возврат то доставка
  end

  def address_client
    # вернет адресс клиента, куда нужно доставить или забрать груз
    return origin_address if is_from_client_to_stock
    # !is_from_stock_to_client && !is_from_client_to_stock "У заказа в адресах не указан склад, по этому он будет считаться доставкой клиенту (а не возвратом)"
    return destination_address
  end
end
