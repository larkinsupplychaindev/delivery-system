class ApplicationRecord < ActiveRecord::Base
  DATE_FORMAT = '%m/%d/%Y'
  self.abstract_class = true

  def self.date_f
    DATE_FORMAT
  end
end
