class Truck < ApplicationRecord
  belongs_to :default_driver, :class_name => 'User', :foreign_key => 'default_driver_id', :validate => true

  def self.available_on_date_and_time_shift(date, time_shift, truck_id = nil)
    date ||= nil
    time_shift ||= nil

    return Truck.none unless date && time_shift && time_shift != ''

=begin
  | - означает включительно start - дата начала периода end - дата окончания периода (смены)
 start             end
  | 1 смена Truck 1 |
                    | 2 смена               |
               truck_start                                                       truck_end
                    | обслуживания/загрузка | 3 смена Truck 1 | обслуживания/загрузка |
                                                              |    ночь     |
                                                                            | 1 смена |
                                                                                      | 2 смена Truck 1 |
                                                                                                        | 3 смена |

  все это не для себя, а для других разработчиков
  условие на естественном языке: если дата начала периода и дата окончания периода смены для перевозки X попадают
  в период планируемой перевозки с учетом того что фуру необходимо обсдужить и загрузить (т.е. -1 одна смена от планируемой и +1 смена к планируемой)
  то такие грузовики не доступны для создания перевозки с указанной датой и временным сдвигом

  Если предположить, что грузовики обслуживаются и загружатся в течении той же смены, когда выполнят рейс, то truck_start и truck_end будут равны периоды смены
  тогда можно будет отправлять грузовики в рейс каждую смену при условии найма дополнительных водителей (так как водители все таки должны отдыхать)
=end

# TODO: запрос ниже не оптимален по определению, его лучше переделать на join между trips и delivery_shifts
# (на конкретный временной сдвиг указанный в параметре) и избавиться от использования функции get_timestamp_by_shift,
# а просто сразу прибавлять интервалы + можно избавиться от одного and если вместо проверки start end смены просто проверять середину периода смены.

    @trucks = Truck.where("not exists(
select 1
from trips tr
where tr.truck_id = trucks.id
  and public.get_timestamp_by_shift(tr.date, tr.time_shift_code, 'start')
    between public.get_timestamp_by_shift(:date, :time_shift_code, 'truck_start')
        and public.get_timestamp_by_shift(:date, :time_shift_code, 'truck_end')
  and public.get_timestamp_by_shift(tr.date, tr.time_shift_code, 'end')
    between public.get_timestamp_by_shift(:date, :time_shift_code, 'truck_start')
        and public.get_timestamp_by_shift(:date, :time_shift_code, 'truck_end')
)", {date: date, time_shift_code: time_shift})

    @trucks = @trucks.where(id: truck_id) if truck_id

    @trucks
  end
end
