class OrderDelivery < ApplicationRecord
  belongs_to :order
  belongs_to :delivery_shift, :class_name => 'DeliveryShift', :foreign_key => 'delivery_shift_code', :validate => true
  has_one :trip_cargo

  def self.available_delivery_on_date_and_time_shift(date, time_shift, delivery_ids = nil)
    date ||= nil
    time_shift ||= nil

    return OrderDelivery.none unless date && time_shift && time_shift != ''

    @deliveries = OrderDelivery.left_outer_joins(:trip_cargo).references(:trip_cargo)
                      .where(delivery_date: date, delivery_shift_code: time_shift, mode: 'TRUCKLOAD', trip_cargos: {id: nil})

    @deliveries = @deliveries.where :id => delivery_ids if delivery_ids

    @deliveries
  end

  def invalid
    @invalid_message = 'not specified: '

    @invalid_message += ' delivery date' unless delivery_date
    @invalid_message += ' delivery shift' unless delivery_shift_code
  end

  def invalid_message
    @invalid_message ||= ''
  end

  def delivery_date_str
    return '' unless delivery_date
    delivery_date.strftime(ApplicationRecord.date_f)
  end
end
