class Trip < ApplicationRecord
  belongs_to :time_shift, :class_name => 'DeliveryShift', :foreign_key => 'time_shift_code', :validate => true
  belongs_to :truck
  belongs_to :driver, :class_name => 'User', :foreign_key => 'driver_id', :validate => true
  has_many :cargo, :class_name => 'TripCargo'

  def check_volume(notices, new_deliveries = nil)
    cargo_items = self.cargo.
        joins(order_delivery: [{order: [:origin_address, :destination_address]}]).
        #select(order_delivery: [:volume, order: [:number, origin_address: :name, destination_address: :name]]).
        select('trip_cargos.id, trip_cargos.order_delivery_id, order_deliveries.id, order_deliveries.volume,
 order_deliveries.order_id, orders.id, orders.number, orders.origin_address_id, addresses.name, orders.destination_address_id, destination_addresses_orders.name').
        order :order_num

    is_error = false
    max_cubes = self.truck.max_cubes
    volume_loaded_on_stock = 0

    cargo_items.each { |c|
      is_from_stock_to_client = Address.address_stock_names.include?(c.order_delivery.order.origin_address.name) # доставка
      is_from_client_to_stock = Address.address_stock_names.include?(c.order_delivery.order.destination_address.name) # возврат

      if !is_from_stock_to_client && !is_from_client_to_stock
        notices.warning "У заказа #{c.order_delivery.order.number} в адресах не указан склад, по этому он будет считаться доставкой клиенту (а не возвратом)"
      end

      volume_loaded_on_stock += c.order_delivery.volume unless is_from_client_to_stock
      if max_cubes < volume_loaded_on_stock
        is_error = true
        notices.error "Заказ #{c.order_delivery.order.number} с объемом #{c.order_delivery.volume}ft3 не помещается, превыщен объем (#{volume_loaded_on_stock}ft3/#{max_cubes}ft3)"
      end
    }

    if new_deliveries
      new_deliveries.each { |d|
        is_from_stock_to_client = Address.address_stock_names.include?(d.order.origin_address.name) # доставка
        is_from_client_to_stock = Address.address_stock_names.include?(d.order.destination_address.name) # возврат

        if !is_from_stock_to_client && !is_from_client_to_stock
          notices.warning "У заказа #{d.order.number} в адресах не указан склад, по этому он будет считаться доставкой клиенту (а не возвратом)"
        end

        volume_loaded_on_stock += d.volume unless is_from_client_to_stock
        if max_cubes < volume_loaded_on_stock
          is_error = true
          notices.error "Заказ #{d.order.number} с объемом #{d.volume}ft3 не помещается, превыщен объем (#{volume_loaded_on_stock}ft3/#{max_cubes}ft3)"
        end
      }
    end

    volume_current = volume_loaded_on_stock

    cargo_items.each { |c|
      is_from_client_to_stock = Address.address_stock_names.include?(c.order_delivery.order.destination_address.name) # возврат

      volume_current -= c.order_delivery.volume unless is_from_client_to_stock
      if is_from_client_to_stock
        volume_current += c.order_delivery.volume

        if max_cubes < volume_current
          is_error = true
          notices.error "Заказ #{c.order_delivery.order.number} на возврат с объемом #{c.order_delivery.volume}ft3 не помещается (предварительно необходимо сделать доставку) (#{volume_current}/#{max_cubes})"
        end
      end
    }

    if new_deliveries
      new_deliveries.each { |d|
        is_from_client_to_stock = Address.address_stock_names.include?(d.order.destination_address.name) # возврат

        volume_current -= d.volume unless is_from_client_to_stock
        if is_from_client_to_stock
          volume_current += d.volume

          if max_cubes < volume_current
            is_error = true
            notices.error "Заказ #{d.order.number} на возврат с объемом #{d.volume}ft3 не помещается (предварительно необходимо сделать доставку) (#{volume_current}/#{max_cubes})"
          end
        end
      }
    end

    return is_error
  end

  def volumes(re_calculate = false)
    @volumes ||= nil
    return @volumes if @volumes && !re_calculate

    sums = cargo.all.
        joins(order_delivery: [{order: [:origin_address, :destination_address]}]).
        select("case
when destination_addresses_orders.name in (#{Address.address_stock_names_to_s_sql}) then 1
else 0 end as is_from_client_to_stock,
sum(order_deliveries.volume) as s").
        group('is_from_client_to_stock').
        order('is_from_client_to_stock')

    is_from_stock_to_client = 0
    is_from_client_to_stock = 0
    sums.each do |r|
      is_from_stock_to_client = r[:s] if r[:is_from_client_to_stock] == 0
      is_from_client_to_stock = r[:s] if r[:is_from_client_to_stock] == 1
    end

    return @volumes = {is_from_stock_to_client: is_from_stock_to_client, is_from_client_to_stock: is_from_client_to_stock}
  end
end
