class Address < ApplicationRecord
  ADDRESS_STOCK_NAMES = ['Larkin LLC'] # Название отправителя или получателя, которого считать складом/магазином

  def self.address_stock_names
    ADDRESS_STOCK_NAMES
  end

  def self.address_stock_names_to_s_sql
    address_stock_names.map { |s| "'#{s}'" }.join(',')
  end

  def to_s_address
    "address: #{raw_line_1} city: #{city}\r\n
state: #{state} zip: #{zip} country: #{country}"
  end
end
