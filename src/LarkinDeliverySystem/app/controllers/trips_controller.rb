class TripsController < ApplicationController
  SORT = {}

  before_filter :authenticate_user!

  load_and_authorize_resource

  def index(for_driver = nil)
    @build_path = lambda { |p| pp = p.permit(:p, s: [:f, :d], f: [:utf8, :search, :date, :commit]); for_driver == :for_driver ? trips_my_path(pp) : trips_path(pp) }

    @pagination = Pagination.new(params, :p, 10, 20, &@build_path)

    query = Trip.includes(:time_shift, :truck, :driver).references(:delivery_shift, :truck, :driver)

    if for_driver == :for_driver
      query = query.where driver: current_user
    end

    if params[:f]
      f = params[:f]

      if f[:search] && f[:search] != ''
        query = query.where('(
lower(delivery_shifts.code) like :search
or lower(delivery_shifts.name) like :search
or lower(trucks.name) like :search
or lower(users.email) like :search
)', {search: "%#{f[:search].to_s.downcase}%"})
      end
      if f[:date] && f[:date] != ''
        d = Date.strptime(f[:date], "%Y-%m-%d")
        query = query.where('trips.date BETWEEN :sd AND :ed', {sd: d, ed: d})
      end
    end

    @pagination.total_rows_count = query.count
    query = order(query, SORT, [{:f => 'trips.date', :d => 'd'}, {:f => 'trips.id', :d => 'a'}])
    @trips = query.limit(@pagination.limit).offset(@pagination.offset)
  end

  def index_my
    index :for_driver
    render 'index'
  end

  def show
    @trip = Trip.find(params[:id])
  end

  def export_to_txt
    @trip = Trip.find(params[:id])

    text = "Routing List on #{@trip.date.strftime(ApplicationRecord.date_f)} #{@trip.time_shift.name}\r\n
    \tTruck: #{@trip.truck.name}\tDirver: #{@trip.driver.email}\r\n"

    @trip.cargo.order(:order_num).each do |c|
      delivery = c.order_delivery
      order = c.order_delivery.order
      address_client = c.order_delivery.order.address_client

      text += "\r\n
      #{c.order_num}. #{address_client.to_s_address}\r\n
Delivery: #{delivery.delivery_date_str} #{delivery.delivery_shift.name}\r\n
Order: \##{order.number} Cargo: #{delivery.handling_unit_quantity} #{delivery.handling_unit_type} (#{delivery.volume} ft3)\r\n
Contact Phone# #{address_client.phone_number} (ask: #{address_client.name})\r\n"
    end

    send_data text, :filename => "#{@trip.date.strftime('%Y-%m-%d')}_#{@trip.time_shift.code}.txt"
  end

  def export_to_pdf
    @trip = Trip.find(params[:id])
    render pdf: "#{@trip.date.strftime('%Y-%m-%d')}_#{@trip.time_shift.code}", layout: 'pdf'
  end

  def new
    @trip_params = {}
    @trip = Trip.new
  end

  def edit
    @trip = Trip.find(params[:id])
  end

  def create
    @notices = Notices.new
    @trip_params = trip_params

    unless params[:commit]
      return render 'new'
    end

    @trip = Trip.new(@trip_params)

    @notices.error "The specified date and time shift truck is not available for trip" unless Truck.available_on_date_and_time_shift(@trip.date, @trip.time_shift_code, @trip.truck_id).count == 1
    @notices.error "The specified date and time shift driver is not available for trip" unless User.available_drivers_on_date_and_time_shift(@trip.date, @trip.time_shift_code, @trip.driver_id).count == 1

    if @notices.notices.count == 0 && @trip.save
      redirect_to @trip
    else
      render 'new'
    end
  end

  def update
    @trip = Trip.find(params[:id])

    if @trip.update(trip_params)
      redirect_to @trip
    else
      render 'edit'
    end
  end

  def destroy
    @trip = Trip.find(params[:id])
    @trip.destroy

    redirect_to trips_path
  end


  private

  def trip_params(p = nil)
    p ||= params
    p = p.require(:trip).permit(:date, :time_shift_code, :truck_id, :driver_id)

    p
  end
end
