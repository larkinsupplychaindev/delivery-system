class WelcomeController < ApplicationController
  #before_action :authenticate_member!

  def index
    unless current_user
      return render 'index_not_auth', :layout => 'application_not_autch'
    end

    if current_user.roles.find_by name: 'Admin'
      return redirect_to trucks_url
    elsif current_user.roles.find_by name: 'Dispatcher'
      return redirect_to orders_url
    elsif current_user.roles.find_by name: 'Driver'
      return redirect_to trips_my_path
    end
  end
end
