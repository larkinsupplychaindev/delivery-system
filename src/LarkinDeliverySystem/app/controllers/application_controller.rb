class ApplicationController < ActionController::Base
  SORT_DEST = {'a' => 'asc', 'd' => 'desc'}

  protect_from_forgery with: :exception

  before_filter do
    resource = controller_name.singularize.to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end

  #before_action :authenticate

  protected

  def order(query, mapping, order)
    unless params[:s]
      params[:s] = order if order.instance_of? Array
      params[:s] = [order] if order.instance_of? Hash
    end

    order = params[:s].map { |s| f = s[:f]; "#{mapping[f]||f} #{SORT_DEST[s[:d]]}" }.join(',') if params[:s]
    query.order order # TODO: Убедиться что тут не может быть SQL иньекции
  end

  private

  # работает не так как нужно + часто повторно запрашивает пароль
  #USERS = { 'test@larkin.com' => '1qaz!QAZ' }
  # def authenticate
  #   authenticate_or_request_with_http_digest do |username|
  #     USERS[username]
  #   end
  # end
end
