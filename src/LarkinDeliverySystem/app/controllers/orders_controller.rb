class OrdersController < ApplicationController
  require 'csv'

  SORT = {
      'next_delivery_date_or_last_delivered_date' => 'coalesce(d42.next_delivery_date_not_specified, coalesce(d42.next_delivery_date, coalesce(d42.last_delivered_date, \'1914-12-25\')))',
      'o_name' => 'addresses.name', # addresses - это Active Record так alias объвляет
      'd_name' => 'destination_addresses_orders.name', # destination_addresses_orders - это Active Record так alias объвляет
      'o_phone' => 'addresses.phone_number',
      'd_phone' => 'destination_addresses_orders.phone_number',
      'o_address' => 'addresses.raw_line_1',
      'd_address' => 'destination_addresses_orders.raw_line_1',
      'o_city' => 'addresses.city',
      'd_city' => 'destination_addresses_orders.city',
      'o_state' => 'addresses.state',
      'd_state' => 'destination_addresses_orders.state',
      'o_zip' => 'addresses.zip',
      'd_zip' => 'destination_addresses_orders.zip',
      'o_country' => 'addresses.country',
      'd_country' => 'destination_addresses_orders.country'
  }

  before_filter :authenticate_user!
  load_and_authorize_resource

  def index
    @build_path = lambda { |p| orders_path(p.permit(:p, s: [:f, :d], f: [:utf8, :search, :date, :state, :direct, :commit])) }

    @pagination = Pagination.new(params, :p, 10, 20, &@build_path)

    query = Order.joins(:origin_address, :destination_address)
                .joins('
left outer join
(
  select
    d47.order_id
    ,min
    (
      case
        when tc.order_delivery_id is null then d47.delivery_date
        else null
      end
    ) as next_delivery_date -- самая первая дата доставки (если еще не доставлено)
    ,min
    (
      case
        when tc.order_delivery_id is null and d47.delivery_date is null then coalesce(d47.delivery_date, \'1983-11-10\')
        else null
      end
    ) as next_delivery_date_not_specified -- самая плохая дата доставки, так как не указана
    ,max
    (
      case
        when tc.order_delivery_id is not null then d47.delivery_date
        else null
      end
    ) as last_delivered_date -- дата последней доставки (или запланированной доставки (имется в виду, что включено в груз) - так как процесс дальше планирования не распространияется)
    ,max
    (
      case
        when d47.delivery_date is null or d47.delivery_shift_code is null then 1
        else 0
      end
    ) as invalid -- что то не указано
    ,min
    (
      case
        when tc.order_delivery_id is null then 0
        else 1
      end
    ) as not_planed -- есть не запланированные
    ,min
    (
      case
        when tc.order_delivery_id is null and not (d47.delivery_date is null or d47.delivery_shift_code is null) then 0
        else 1
      end
    ) as not_planed_only_valid -- есть не запланированные, без учета не корректных
    ,max
    (
      case
        when tc.order_delivery_id is not null then 1
        else 0
      end
    ) as planed -- есть запланированные
    ,min
    (
      case
        when tc.order_delivery_id is not null then 1
        else 0
      end
    ) as full_planed -- полностью запланированны
  from order_deliveries as d47
    left join trip_cargos tc on tc.order_delivery_id = d47.id
  group by d47.order_id
) as d42 on d42.order_id = orders.id')
    query = query.references(:origin_address, :destination_address)

    if params[:f]
      f = params[:f]

      if f[:search] && f[:search] != ''
        query = query.where('(
orders.number like :search
or lower(addresses.name) like :search or lower(destination_addresses_orders.name) like :search
or lower(addresses.phone_number) like :search or lower(destination_addresses_orders.phone_number) like :search
or lower(addresses.raw_line_1) like :search or lower(destination_addresses_orders.raw_line_1) like :search
or lower(addresses.city) like :search or lower(destination_addresses_orders.city) like :search
or lower(addresses.state) like :search or lower(destination_addresses_orders.state) like :search
or lower(addresses.zip) like :search or lower(destination_addresses_orders.zip) like :search
or lower(addresses.country) like :search or lower(destination_addresses_orders.country) like :search
)', {search: "%#{f[:search].to_s.downcase}%"})
      end

      if f[:date] != ''
        d = Date.strptime(f[:date], "%Y-%m-%d")
        query = query.where('exists(select 1 from order_deliveries as d47_2 where d47_2.delivery_date BETWEEN :sd AND :ed and d47_2.order_id = orders.id)', {sd: d, ed: d})
      end

      if f[:state] == 'invalid'
        query = query.where('d42.invalid = 1')
      elsif f[:state] == 'not_planed'
        query = query.where('d42.not_planed = 0')
      elsif f[:state] == 'not_planed_only_valid'
        query = query.where('d42.not_planed_only_valid = 0')
      elsif f[:state] == 'planed'
        query = query.where('d42.planed = 1')
      elsif f[:state] == 'full_planed'
        query = query.where('d42.full_planed = 1')
      end

      if f[:direct] == 'from_stock'
        query = query.where("addresses.name in (#{Address.address_stock_names_to_s_sql})")
      elsif f[:direct] == 'to_stock'
        query = query.where("destination_addresses_orders.name in (#{Address.address_stock_names_to_s_sql})")
      end
    end

    @pagination.total_rows_count = query.count

    query = query.select('d42.next_delivery_date, d42.next_delivery_date_not_specified, d42.last_delivered_date, orders.*') #orders.* - фикс ActiveRecords, генерит кривой sql и потом еще не может найти поля
    query = order(query, SORT, [{:f => 'next_delivery_date_or_last_delivered_date', :d => 'a'}, {:f => 'orders.id', :d => 'a'}])
    @orders = query.limit(@pagination.limit).offset(@pagination.offset)
  end

  def show
    @order = Order.includes(:origin_address, :destination_address).find params[:id]
  end

  def upload_view
    params[:not_download_all] = 'not_download_all'
  end

  def upload
    @notices = Notices.new

    begin
      fields = ['delivery_date', 'delivery_shift', 'origin_name', 'origin_raw_line_1', 'origin_city', 'origin_state',
                'origin_zip', 'origin_country', 'client name', 'destination_raw_line_1', 'destination_city',
                'destination_state', 'destination_zip', 'destination_country', 'phone_number', 'mode',
                'purchase_order_number', 'volume', 'handling_unit_quantity', 'handling_unit_type']

      uploaded_io = params[:file]

      if uploaded_io.nil?
        raise 'Unknown file'
      end

      if File.extname(uploaded_io.original_filename) != '.csv'
        raise 'File extension must be csv'
      end

      csv_text = uploaded_io.read()
      csv = CSV.parse(csv_text, :headers => true)

      not_found_fields = fields - csv.headers
      if not_found_fields != []
        raise 'File format is not valid. Not found columns: ' + not_found_fields.map(&:inspect).join(', ')
      end

      rollback = false
      Order.transaction do
        csv.each_with_index do |row, row_index|
          row_line = row_index + 2
          # TODO: Добавить предварительную валидацию полей (или на основе модели валидации ActiveRecords), и сохранение с пропуском не валидных полей

          unless row['purchase_order_number']
            @notices.warning "In line #{row_line} not specified order number - record missing" unless params[:not_download_all] == 'not_download_all'
            raise "In line #{row_line} not specified order number" if params[:not_download_all] == 'not_download_all'
            next
          end

          order = Order.find_by number: row['purchase_order_number']
          order = order || Order.new #create!(row.to_hash)

          if order.new_record?
            order.number = row['purchase_order_number']
          end

          fill_addresses(order, row)

          order.save! if params[:not_download_all] == 'not_download_all'
          unless params[:not_download_all] == 'not_download_all'
            if !order.save
              @notices.warning "In line #{row_line} error save - record missing (#{order.errors.full_messages.join(',')})"
              next
            end
          end

          #9/15/2014
          delivery_date = nil
          if row['delivery_date']
            delivery_date = Date.strptime(row['delivery_date'], ApplicationRecord.date_f)
          end

          original_key = "d_#{row['delivery_date']}_s_#{row['delivery_shift']}_m_#{row['mode']}"+
              "_v_#{row['volume']}_uq_#{row['handling_unit_quantity']}_ut_#{row['handling_unit_type']}"

          delivery = order.deliveries.find_by original_key: original_key
          delivery = delivery || order.deliveries.new
          if delivery.new_record?
            delivery.original_key = original_key
            delivery.delivery_date = delivery_date
            delivery.delivery_shift_code = row['delivery_shift']
            delivery.mode = row['mode']
            delivery.volume = row['volume']
            delivery.handling_unit_quantity = row['handling_unit_quantity']
            delivery.handling_unit_type = row['handling_unit_type']
          else
            msg = "In line #{row_line} is already loaded"
            raise msg if params[:not_download_all] == 'not_download_all'
            unless params[:not_download_all] == 'not_download_all'
              @notices.warning "#{msg} - record missing"
              next
            end
          end

          order.save! if params[:not_download_all] == 'not_download_all'
          unless params[:not_download_all] == 'not_download_all'
            if !order.save
              @notices.warning "In line #{row_line} error save - record missing (#{order.errors.full_messages.join(',')})"
              next
            end
          end
        end

        if rollback && params[:not_download_all] == 'not_download_all'
          ActiveRecord::Rollback
          raise "Download canceled"
        end
      end

      @notices.success 'successfully'
      return render 'uploaded'

    rescue => error
      puts error.inspect
      @notices.error error.message
    end

    render 'upload_view'
  end


  private

  def fill_addresses(order, row)
    origin_address = order.origin_address
    #'origin_name', 'origin_raw_line_1', 'origin_city', 'origin_state', 'origin_zip', 'origin_country'
    origin_address = origin_address ||
        Address.find_by(name: row['origin_name'], raw_line_1: row['origin_raw_line_1'], city: row['origin_city'],
                        state: row['origin_state'], zip: row['origin_zip'], country: row['origin_country'])
    origin_address = origin_address || Address.new
    order.origin_address = origin_address
    if origin_address.new_record?
      fill_address origin_address, row, 'origin'
    end

    destination_address = order.destination_address
    #'client name', 'destination_raw_line_1', 'destination_city', 'destination_state', 'destination_zip',
    # 'destination_country', 'phone_number'
    destination_address = destination_address ||
        Address.find_by(name: row['client name'], raw_line_1: row['destination_raw_line_1'],
                        city: row['destination_city'], state: row['destination_state'],
                        zip: row['destination_zip'], country: row['destination_country'],
                        phone_number: row['phone_number'])
    destination_address = destination_address || Address.new
    order.destination_address = destination_address
    if destination_address.new_record?
      fill_address destination_address, row, 'destination'
    end
  end

  def fill_address(address, row, prefix)
    if prefix == 'destination'
      address.name = row['client name']
    else
      address.name = row["#{prefix}_name"]
    end

    address.raw_line_1 = row["#{prefix}_raw_line_1"]
    address.city = row["#{prefix}_city"]
    address.state = row["#{prefix}_state"]
    address.zip = row["#{prefix}_zip"]
    address.country = row["#{prefix}_country"]

    if prefix == 'destination'
      address.phone_number = row['phone_number']
    end
  end

end
