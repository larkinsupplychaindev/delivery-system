class TrucksController < ApplicationController
  SORT = {}

  before_filter :authenticate_user!
  load_and_authorize_resource

  def index
    @build_path = lambda { |p| trucks_path(p.permit(:p, s: [:f, :d], f: [:utf8, :search, :date, :commit])) }

    @pagination = Pagination.new(params, :p, 10, 20, &@build_path)

    query = Truck.includes(:default_driver)

    if params[:f]
      f = params[:f]

      if f[:search] && f[:search] != ''
        query = query.references(:comments, :destination_address) # вот так правильно принуждать к постоянному join - при этом слетает поле next_delivery_date
        query = query.where('(
lower(trucks.name) like :search
or to_char(trucks.max_weight, \'FM999999\') like :search
or to_char(trucks.max_cubes, \'FM999999\') like :search
or lower(users.email) like :search
)', {search: "%#{f[:search].to_s.downcase}%"})
      end
    end

    @pagination.total_rows_count = query.count
    query = order(query, SORT, [{:f => 'name', :d => 'a'}])
    @entities = query.limit(@pagination.limit).offset(@pagination.offset)
  end
end
