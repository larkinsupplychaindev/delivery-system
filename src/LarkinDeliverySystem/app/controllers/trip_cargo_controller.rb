class TripCargoController < ApplicationController
  before_filter :authenticate_user!

  load_and_authorize_resource

  def create
    @notices = Notices.new

    begin
      @trip = Trip.find(params[:trip_id])

      delivery_ids = params[:delivery_ids] || []

      raise 'The specified order for include trip' unless delivery_ids.count > 0
      raise 'The specified orders is not available for trip' unless OrderDelivery.available_delivery_on_date_and_time_shift(
          @trip.date, @trip.time_shift_code, delivery_ids).count == delivery_ids.count

      deliveries = OrderDelivery.find delivery_ids

      if @trip.check_volume @notices, deliveries
        @notices.warning 'Select fewer orders or follow the partition with a large volume of orders for several deliveries'
        raise 'The specified orders exceeded the maximum cubes of truck'
      end

      order_num = @trip.cargo.maximum :order_num
      order_num ||= 0

      Trip.transaction do
        deliveries.each do |d|
          @trip.cargo.new :order_delivery => d, :order_num => (order_num += 1)
        end

        @trip.save!
      end

      return redirect_to @trip
    rescue => error
      puts error.inspect
      @notices.error error.message
    end

    render 'trips/show'
  end

  def update
    @notices = Notices.new

    begin
      @trip = Trip.find(params[:trip_id])

      cargo_ids = params[:ids] || []

      raise 'The specified order for exclude from trip' unless cargo_ids.count > 0

      cargo_items = TripCargo.find cargo_ids

      Trip.transaction do
        order_num = 0
        cargo_ids.each do |id|
          order_num += 1 # специально тут
          cargo_item = cargo_items.find { |с| с.id == id.to_i }
          if cargo_item
            cargo_item.order_num = order_num
            cargo_item.save!
          end
        end

        if @trip.check_volume @notices, nil
          @notices.warning 'Select fewer orders or follow the partition with a large volume of orders for several deliveries'
          raise 'The specified orders exceeded the maximum cubes of truck'
        end
      end

      return redirect_to @trip
    rescue => error
      puts error.inspect
      @notices.error error.message
    end

    render 'trips/show'
  end

  def destroy
    @notices = Notices.new

    begin
      @trip = Trip.find(params[:trip_id])

      cargo_ids = params[:cargo_ids] || []

      raise 'The specified order for exclude from trip' unless cargo_ids.count > 0

      cargo_items = TripCargo.find cargo_ids

      Trip.transaction do
        cargo_items.each do |c|
          c.destroy
        end

        order_num = 0
        @trip.cargo.all.order(:order_num).each do |c|
          c.order_num = (order_num += 1)
          c.save!
        end

        #@trip.save! не сохраняет дочерние сущности

        if @trip.check_volume @notices, nil
          @notices.warning 'Select fewer orders or follow the partition with a large volume of orders for several deliveries'
          raise 'The specified orders exceeded the maximum cubes of truck'
        end
      end

      return redirect_to @trip
    rescue => error
      puts error.inspect
      @notices.error error.message
    end

    render 'trips/show'
  end
end
