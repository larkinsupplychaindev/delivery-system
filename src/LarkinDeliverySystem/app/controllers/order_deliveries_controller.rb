class OrderDeliveriesController < ApplicationController

  before_filter :authenticate_user!

  load_and_authorize_resource

  def edit
    @order = Order.find(params[:order_id])
    @orderDelivery = OrderDelivery.find(params[:id])

    session[:return_to] ||= request.referer
  end

  def update
    @notices = Notices.new

    begin
      before_update

      Order.transaction do
        @orderDelivery.update!(order_delivery_params)
      end

      return redirect_to session.delete(:return_to)
    rescue => error
      puts error.inspect
      @notices.error error.message
    end

    render 'edit'
  end

  def split
    @notices = Notices.new

    begin
      before_update

      params.require :new_volume
      params.require :new_unit_quantity
      params.require :new_delivery_date
      params.require :new_delivery_shift_code

      new_origin_volume = @orderDelivery.volume - params[:new_volume].to_d
      raise 'Not correct value of the new volume' unless 0 < new_origin_volume && new_origin_volume < @orderDelivery.volume

      new_origin_unit_quantity = @orderDelivery.handling_unit_quantity - params[:new_unit_quantity].to_i
      raise 'Not correct value of the new unit quantity' unless 1 <= new_origin_unit_quantity && new_origin_unit_quantity < @orderDelivery.handling_unit_quantity

      Order.transaction do
        @order.deliveries.create! original_key: @orderDelivery.original_key, delivery_date: params[:new_delivery_date],
                                  delivery_shift_code: params[:new_delivery_shift_code], mode: @orderDelivery.mode,
                                  volume: params[:new_volume].to_d, handling_unit_quantity: params[:new_unit_quantity].to_i,
                                  handling_unit_type: @orderDelivery.handling_unit_type

        @orderDelivery.volume = new_origin_volume
        @orderDelivery.handling_unit_quantity = new_origin_unit_quantity
        @orderDelivery.delivery_date = order_delivery_params[:delivery_date]
        @orderDelivery.delivery_shift_code = order_delivery_params[:delivery_shift_code]

        @orderDelivery.save!
      end

      return redirect_to session.delete(:return_to)
    rescue => error
      puts error.inspect
      @notices.error error.message
    end

    render 'edit'
  end

  def before_update
    @order = Order.find(params[:order_id])
    @orderDelivery = OrderDelivery.find(params[:id])

    raise 'Said delivery can not be changed, since it is included in the trip' unless @orderDelivery.trip_cargo.nil?
  end

  private

  def order_delivery_params(p = nil)
    p ||= params
    p = p.require(:order_delivery).permit(:delivery_date, :delivery_shift_code)

    p
  end
end
