# README

* System dependencies
    * Rubi и Rails
    * PostgreSQL
    * wkhtmltopdf    
    Скачать http://wkhtmltopdf.org/downloads.html#stable    
    и поставить для нужной OS
 
* Configuration
    * wkhtmltopdf   
    в config/initializers/wicked_pdf.rb     
    прописать путь к wkhtmltopdf.exe или его каталогу (в windows полный путь к exe)
    например: exe_path: 'C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe'
    
* База данных (нужен установленный PostgreSQL)
     * Создание роли    
    Не забываем добавить путь к bin диретории PostgreSQL в Path.   
    В консоли выполнить:    
    psql -U postgres    
    ввести пароль
    после успешного логина, выполнить создание роли:    
    create role LarkinDeliverySystem with createdb login password 'qwe';    
    'qwe' - пароль на свое усмотрение, но потом прописать в config/database.yml (да и вобще проверить параметры подключения)

    * Для создания БД и инициализации, выполнить rake task:   
    db:create   
    db:migrate  
    db:seed  
    !!! команду db:setup - не выполнять, так как накатывает из shema.db,
    а она по умолчанию строится не корректно (не учитвается тип INTERVAL и в ней нет sql функций)
    
    * Если нужно переразвернуть заново, необходимо закрыть все подключения к БД и выполнить rake task:      
    db:drop (или БД удалить руками)     
    db:create   
    db:migrate

* Возможные проблемы:
    * Ошибка: Не возмиожно загрузить bcrypt...  
    то https://github.com/codahale/bcrypt-ruby/issues/142   
    выполнить:  
        * gem uninstall bcrypt    
    (and select option 3 (if exist) and uninstall all bcrypt-ruby gem versions with)       
        * gem uninstall bcrypt-ruby   
    and select option 3 (if exist) then install bcrypt using    
        * gem install bcrypt --platform=ruby
        
* How to run the test suite     
    Тесты не делал 
    
* Тестовые пользователи
    * dispatcher1@larkin.com/1qaz!QAZ
    * driver1@larkin.com/1qaz!QAZ
    * driver2@larkin.com/1qaz!QAZ