require_relative 'boot'

require 'rails/all'

require File.expand_path('../boot', __FILE__)

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module LarkinDeliverySystem
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.autoload_paths << Rails.root.join("app/models/simple_models")

    config.assets.precompile += %w(*.png *.jpg *.jpeg *.gif)

    config.to_prepare do
      Devise::SessionsController.layout "application_not_autch"
      Devise::RegistrationsController.layout "application_not_autch"
      Devise::PasswordsController.layout "application_not_autch"
    end
  end
end
