Rails.application.routes.draw do
  get 'trucks/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  get '/orders/upload', to: 'orders#upload_view'
  post '/orders/upload', to: 'orders#upload'
  resources :orders, only: [:index, :show, :upload_view, :upload] do
    put '/order_deliveries/:id', to: 'order_deliveries#split'
    resources :order_deliveries, only: [:edit, :update] # знаю, знаю стоило OrderDelivery - назвать просто Delivery
  end

  get '/trips/my', to: 'trips#index_my'
  get '/trips/:id/txt', to: 'trips#export_to_txt', as: 'trip_txt'
  get '/trips/:id/pdf', to: 'trips#export_to_pdf', as: 'trip_pdf'
  resources :trips do
    put :cargo, to: 'trip_cargo#update'
    delete :cargo, to: 'trip_cargo#destroy'
    resources :cargo, :controller => :trip_cargo, only: [:create] # знаю, знаю стоило TripCargo - назвать просто Cargo
  end

  resources :trucks, only: [:index]

  devise_for :users

  root 'welcome#index'
end
