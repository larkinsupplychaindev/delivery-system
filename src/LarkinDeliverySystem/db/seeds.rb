# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

unless DeliveryShift.find_by_code :M
  DeliveryShift.create!(:code => :M, :name => 'Morning 8am - 12 pm', :order => 10,
                        :interval_start => '8 hours', :interval_end => '12 hours',
                        # учет того что водитель должен отдыхать между сменами
                        :interval_driver_start => '-6 hours', :interval_driver_end => '18 hours',
                        # учет того что фуру необходимо обсдужить и загрузить
                        :interval_truck_start => '-6 hours', :interval_truck_end => '18 hours')
end
unless DeliveryShift.find_by_code :N
  DeliveryShift.create!(:code => :N, :name => 'Afternoon 12pm - 6pm', :order => 20,
                        :interval_start => '12 hours', :interval_end => '18 hours',
                        # учет того что водитель должен отдыхать между сменами
                        :interval_driver_start => '8 hours', :interval_driver_end => '22 hours',
                        # учет того что фуру необходимо обсдужить и загрузить
                        :interval_truck_start => '8 hours', :interval_truck_end => '22 hours')
end
unless DeliveryShift.find_by_code :E
  DeliveryShift.create!(:code => :E, :name => 'Evening 6pm - 10 pm', :order => 30,
                        :interval_start => '18 hours', :interval_end => '22 hours',
                        # учет того что водитель должен отдыхать между сменами
                        :interval_driver_start => '12 hours', :interval_driver_end => '32 hours',
                        # учет того что фуру необходимо обсдужить и загрузить
                        :interval_truck_start => '12 hours', :interval_truck_end => '32 hours')
end

def create_role(name)
  role = Role.find_by_name name
  role = role || Role.create!(:name => name)
  role
end

def create_user(email, password, roles)
  user = User.find_by_email email
  user = user || User.create!(:email => email, :password => password)

  roles.each do |role|
    user.users_roles.create!(:role => role) unless user.users_roles.find_by :role => role
  end

  user
end

def create_truck(name, max_weight, max_cubes, default_driver)
  truck = Truck.find_by_name name
  truck = truck || Truck.create!(:name => name, :max_weight => max_weight, :max_cubes => max_cubes, :default_driver => default_driver)
  truck
end

role_admin = create_role :Admin
role_dispatcher = create_role :Dispatcher
role_driver = create_role :Driver


create_user 'test@larkin.com', '1qaz!QAZ', [role_admin, role_dispatcher, role_driver]

create_user 'admin@larkin.com', '1qaz!QAZ', [role_admin]
create_user 'dispatcher1@larkin.com', '1qaz!QAZ', [role_dispatcher]
driver1 = create_user 'driver1@larkin.com', '1qaz!QAZ', [role_driver]
driver2 = create_user 'driver2@larkin.com', '1qaz!QAZ', [role_driver]
#create_user 'driver3@larkin.com', '1qaz!QAZ', [role_driver]

create_truck 'Truck 1 Goby', 10000, 1400, driver1
create_truck 'Truck 2 Gazelle', 10000, 1400, driver2


