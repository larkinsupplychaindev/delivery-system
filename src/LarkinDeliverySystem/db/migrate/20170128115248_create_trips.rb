class CreateTrips < ActiveRecord::Migration[5.0]
  def change
    create_table :trips do |t|
      t.datetime :date, :null => false
      t.string :time_shift_code, limit: 1, :null => false
      t.references :truck, Truck: true, foreign_key: true, :null => false
      t.integer :driver_id, :null => false

      t.timestamps
    end
    add_foreign_key :trips, :delivery_shifts, column: :time_shift_code, primary_key: :code, name: :fk_trips_delivery_shifts_time_shift_code
    add_foreign_key :trips, :users, column: :driver_id, name: :fk_trips_users_driver_id
  end
end
