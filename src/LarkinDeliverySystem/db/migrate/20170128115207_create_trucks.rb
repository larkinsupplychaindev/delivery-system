class CreateTrucks < ActiveRecord::Migration[5.0]
  def change
    create_table :trucks do |t|
      t.string :name, limit: 50, :null => false
      t.decimal :max_weight, precision: 10, scale: 0, :null => false
      t.decimal :max_cubes, precision: 10, scale: 0, :null => false
      t.integer :default_driver_id, :null => true

      t.timestamps
    end
    add_foreign_key :trucks, :users, column: :default_driver_id, name: :fk_trucks_users_default_driver_id
  end
end
