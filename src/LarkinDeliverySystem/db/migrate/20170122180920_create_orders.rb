class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.string :number, limit: 15, :null => false
      t.integer :origin_address_id, :null => false
      t.integer :destination_address_id, :null => false

      t.timestamps
    end
    add_index :orders, :number, unique: true
    add_foreign_key :orders, :addresses, column: :origin_address_id, name: :fk_orders_addresses_origin_address_id
    add_foreign_key :orders, :addresses, column: :destination_address_id, name: :fk_orders_addresses_destination_address_id
  end
end
