class CreateDeliveryShifts < ActiveRecord::Migration[5.0]
  def change
    #create_table :delivery_shifts, id: false, primary_key: :code do |t|
    create_table :delivery_shifts, primary_key: :code, id: :string, limit: 1 do |t|
      #t.string :code, limit: 1
      t.string :name, limit: 50, :null => false
      t.integer :order, :null => false

      # если к дате прибавить данный интервал, то полуим дату и время начала смены (временного сдвига)
      t.column :interval_start, :interval, :null => false
      # если к дате прибавить данный интервал, то полуим дату и время окончания смены (временного сдвига)
      t.column :interval_end, :interval, :null => false

      # если к дате прибавить данный интервал, то полуим дату и время с которого водитель не должен работать перед началом смены
      # учет того что водитель должен отдыхать между сменами
      t.column :interval_driver_start, :interval, :null => false
      # если к дате прибавить данный интервал, то полуим дату и время до которого водитель не должен рабтать после окончания смены
      # учет того что водитель должен отдыхать между сменами
      t.column :interval_driver_end, :interval, :null => false

      # если к дате прибавить данный интервал, то полуим дату и время с которого фура не должна работать перед началом смены
      # учет того что фуру необходимо обсдужить и загрузить
      t.column :interval_truck_start, :interval, :null => false
      # если к дате прибавить данный интервал, то полуим дату и время до которого фура не должна рабтать после окончания смены
      # учет того что фуру необходимо обсдужить и загрузить
      t.column :interval_truck_end, :interval, :null => false

      t.timestamps
    end
    #execute "ALTER TABLE delivery_shifts ADD PRIMARY KEY (code);"

    execute(%q{
CREATE OR REPLACE FUNCTION public.get_timestamp_by_shift(
	date timestamp without time zone,
	shift_code character varying(1),
	interval_code character varying(12))
    RETURNS timestamp without time zone
    LANGUAGE 'sql'
    COST 100.0
    VOLATILE NOT LEAKPROOF
AS $function$

select date + (case
	when interval_code = 'start' then ds.interval_start
	when interval_code = 'end' then ds.interval_end
	when interval_code = 'truck_start' then ds.interval_truck_start
	when interval_code = 'truck_end' then ds.interval_truck_end
	when interval_code = 'driver_start' then ds.interval_driver_start
	when interval_code = 'driver_end' then ds.interval_driver_end
    else null
    end)
from public.delivery_shifts ds
where ds.code = shift_code

$function$;
  })
  end
end
