class CreateTripCargos < ActiveRecord::Migration[5.0]
  def change
    create_table :trip_cargos do |t|
      t.references :trip, Trip: true, foreign_key: true, :null => false
      t.integer :order_num, :null => false
      t.references :order_delivery, OrderDelivery: true, foreign_key: true, :null => false

      t.timestamps
    end
  end
end
