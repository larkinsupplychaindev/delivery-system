class CreateOrderDeliveries < ActiveRecord::Migration[5.0]
  def change
    create_table :order_deliveries do |t|
      t.references :order, foreign_key: true, :null => false
      t.string :original_key, limit: 50, :null => false
      t.datetime :delivery_date
      t.string :delivery_shift_code, limit: 1
      t.string :mode, limit: 50
      t.decimal :volume, precision: 10, scale: 2
      t.integer :handling_unit_quantity
      t.string :handling_unit_type, limit: 50

      t.timestamps
    end
    add_foreign_key :order_deliveries, :delivery_shifts, column: :delivery_shift_code, primary_key: :code, name: :fk_order_deliveries_delivery_shifts_delivery_shift_code
  end
end
