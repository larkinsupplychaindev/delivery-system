class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.string :name, limit: 100
      t.string :raw_line_1, limit: 200
      t.string :city, limit: 50
      t.string :state, limit: 50
      t.string :zip, limit: 50
      t.string :country, limit: 50
      t.string :phone_number, limit: 50

      t.timestamps
    end
  end
end
