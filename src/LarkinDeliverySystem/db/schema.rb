# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170128115401) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "name", limit: 100
    t.string "raw_line_1", limit: 200
    t.string "city", limit: 50
    t.string "state", limit: 50
    t.string "zip", limit: 50
    t.string "country", limit: 50
    t.string "phone_number", limit: 50
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "delivery_shifts", primary_key: "code", id: :string, limit: 1, force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.integer "order", null: false
    t.string "interval_start", null: false
    t.string "interval_end", null: false
    t.string "interval_driver_start", null: false
    t.string "interval_driver_end", null: false
    t.string "interval_truck_start", null: false
    t.string "interval_truck_end", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "order_deliveries", force: :cascade do |t|
    t.integer "order_id", null: false
    t.string "original_key", limit: 50, null: false
    t.datetime "delivery_date"
    t.string "delivery_shift_code", limit: 1
    t.string   "mode",                   limit: 50
    t.decimal  "volume",                            precision: 10, scale: 2
    t.integer  "handling_unit_quantity"
    t.string   "handling_unit_type",     limit: 50
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.index ["order_id"], name: "index_order_deliveries_on_order_id", using: :btree
  end

  create_table "orders", force: :cascade do |t|
    t.string "number", limit: 15, null: false
    t.integer "origin_address_id", null: false
    t.integer "destination_address_id", null: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["number"], name: "index_orders_on_number", unique: true, using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "trip_cargos", force: :cascade do |t|
    t.integer "trip_id", null: false
    t.integer "order_num", null: false
    t.integer "order_delivery_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_delivery_id"], name: "index_trip_cargos_on_order_delivery_id", using: :btree
    t.index ["trip_id"], name: "index_trip_cargos_on_trip_id", using: :btree
  end

  create_table "trips", force: :cascade do |t|
    t.datetime "date", null: false
    t.string "time_shift_code", limit: 1, null: false
    t.integer "truck_id", null: false
    t.integer "driver_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["truck_id"], name: "index_trips_on_truck_id", using: :btree
  end

  create_table "trucks", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.decimal "max_weight", precision: 10, null: false
    t.decimal "max_cubes", precision: 10, null: false
    t.integer "default_driver_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "users_roles", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "role_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_users_roles_on_role_id", using: :btree
    t.index ["user_id"], name: "index_users_roles_on_user_id", using: :btree
  end

  add_foreign_key "order_deliveries", "delivery_shifts", column: "delivery_shift_code", primary_key: "code", name: "fk_order_deliveries_delivery_shifts_delivery_shift_code"
  add_foreign_key "order_deliveries", "orders"
  add_foreign_key "orders", "addresses", column: "destination_address_id", name: "fk_orders_addresses_destination_address_id"
  add_foreign_key "orders", "addresses", column: "origin_address_id", name: "fk_orders_addresses_origin_address_id"
  add_foreign_key "trip_cargos", "order_deliveries"
  add_foreign_key "trip_cargos", "trips"
  add_foreign_key "trips", "delivery_shifts", column: "time_shift_code", primary_key: "code", name: "fk_trips_delivery_shifts_time_shift_code"
  add_foreign_key "trips", "trucks"
  add_foreign_key "trips", "users", column: "driver_id", name: "fk_trips_users_driver_id"
  add_foreign_key "trucks", "users", column: "default_driver_id", name: "fk_trucks_users_default_driver_id"
  add_foreign_key "users_roles", "roles"
  add_foreign_key "users_roles", "users"
end
